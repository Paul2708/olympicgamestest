# Olympic Games Tester
--------------------------------------------------
This project is a small test framework for the final task in 'Programming'.  
Note: The framework is not the main task. It is not as clean as it should be!  
Warning: As the test will call the `main`-method multiple times, static variables won't change!
## Features
* input and output testing
* configure and create own tests
* simple and clean way to add tests
* detailed feedback on possible failures

## Setup
* Download the repository [here](https://bitbucket.org/Paul2708/olympicgamestest/downloads/).
* Copy the downloaded folder `.../src/test` into your own `src` folder.
* Replace your `Terminal.java` with the downloaded file `.../src/edu/kit/informatik/Terminal.java`.
* Set your main class in `test/Tester#MAIN`.
* Rebuild your project.

## How to test
Run `test/Tester.java` in your project by your IDE.

## Add new tests
You can create new tests by adding a new `TestCase`. Just follow the examples.

## Examples
#### Sample test
```java
package test.custom;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "Praktomat test")
public class PublicTest extends TestCase {

    @Test(desc = "public test")
    public void test() {
        print("add-admin peter;oettig;peet1993;notmyrealpw");
        expect("OK");
        print("login-admin peet1993;notmyrealpw");
        expect("OK");
        print("add-olympic-sport biathlon;sprint");
        expect("OK");
        print("add-ioc-code 111;ger;deutschland;1990");
        expect("OK");
        print("add-athlete 1234;laura;dahlmeier;deutschland;biathlon;sprint");
        expect("OK");
        print("add-competition 1234;2018;deutschland;biathlon;sprint;1;0;0");
        expect("OK");
        print("summary-athletes biathlon;sprint");
        expect("1234 laura dahlmeier 1");
        print("olympic-medal-table");
        expect("(1,111,ger,deutschland,1,0,0,1)");
        print("quit");
        expectQuit();
    }
}
```
#### Sample output (passed)
```
Running tests.. 
========================
Test unit: Praktomat test
Test: public test
  Test passed. (22ms)
========================

You passed 1/1 tests and failed 0 test(s).
```
#### Sample output (failed)
```
Running tests.. 
========================
Test unit: Praktomat test
Test: public test
 Test failed. (17ms)
     Lookup: test.custom.PublicTest
     Line: olympic-medal-table
     Expected: (1,111,ger,deutschland,1,0,0,1)
     Found: (0,111,ger,deutschland,0,0,0,0)
========================

You passed 0/1 tests and failed 1 test(s).
```
## Updates
Just check out the [commits](https://bitbucket.org/Paul2708/olympicgamestest/commits/all) to be up-to-date!