package test;

public class TestExpectation {

    public static enum Type {
        
        TEXT,
        ERROR,
        QUIT;
    }
    
    private String command;
    private Type expectedType;
    private String expectedText;
    
    public TestExpectation(String command, Type expectedType, String expectedText) {
        this.command = command;
        this.expectedType = expectedType;
        this.expectedText = expectedText;
    }
    
    public TestExpectation(String command, Type expectedType) {
        this(command, expectedType, null);
    } 
    
    public String getCommand() {
        return command;
    }
    
    public Type getExpectedType() {
        return expectedType;
    }
    
    public String getExpectedText() {
        return expectedText;
    }
}
