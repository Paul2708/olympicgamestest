package test;

import java.util.ArrayList;
import java.util.List;

import test.TestExpectation.Type;

public class TestCase {
    
    private List<TestExpectation> expectations;
    
    private String command;
    
    public TestCase() {
        this.expectations = new ArrayList<>();
    }
    
    // Basic
    public void print(String line) {
        this.command = line;
    }
    
    public void expect(String expected) {
        TestExpectation expectation;
        if (expected.equals("Error, ")) {
            expectation = new TestExpectation(command, Type.ERROR);
        } else if (expected.equals("quit")) {
            expectation = new TestExpectation(command, Type.QUIT);
        } else {
            expectation = new TestExpectation(command, Type.TEXT, expected);
        }
        
        this.expectations.add(expectation);
    }
    
    // Helpful
    public void expectError() {
        expect("Error, ");
    }
    
    public void expectQuit() {
        expect("quit");
    }
    
    public void quit() {
        print("quit");
        expectQuit();
    }
    
    public void login() {
        print("add-admin max;mustermann;maxi;123456789");
        expect("OK");
        print("login-admin maxi;123456789");
        expect("OK");
    }
    
    public void logout() {
        print("logout-admin");
        expect("OK");
    }
    
    public void reset() {
        this.expectations.clear();
        this.command = null;
    }
    
    public List<TestExpectation> getExpectations() {
        return expectations;
    }
}
