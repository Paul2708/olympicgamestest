package test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import edu.kit.informatik.Terminal;
import test.annotation.Test;
import test.annotation.TestUnit;

public class TestManager {
    
    private List<TestCase> tests;
    private List<TestResult> results;
    
    public TestManager() {
        this.tests = new ArrayList<>();
        this.results = new ArrayList<>();
        
        Terminal.testing();
    }

    public void loadTestCases() {
        // System.out.println("Loading test files...");

        try {
            Class<?>[] classes = getClasses("test");

            for (Class<?> clazz : classes) {
                if (TestCase.class.isAssignableFrom(clazz) && !TestCase.class.equals(clazz) && clazz.isAnnotationPresent(TestUnit.class)) {
                    tests.add((TestCase) clazz.newInstance());
                }
            }
        } catch (Exception e) {
            // System.err.println("Failed to load all tests. (" + e.getMessage() + ")");
            e.printStackTrace();
        }

        // System.out.println(tests.size() + " test files loaded.");
    }

    public void runTestCases() {
        System.out.println("Running tests.. ");
        
        System.out.println("========================");
        
        for (TestCase test : tests) {
            String header = test.getClass().getAnnotation(TestUnit.class).header();
            
            System.out.println("Test unit: " + header);

            for (Method method : sort(test.getClass())) {
                if (method.isAnnotationPresent(Test.class)) {
                    String description = method.getAnnotation(Test.class).desc();
                    
                    System.out.println("Test: " + description);

                    // Call method
                    try {
                        method.invoke(test);
                    } catch (Exception e) {
                        System.err.println("Invoking method " + method.getName() + " failed. (" + e.getMessage() + ")");
                        e.printStackTrace();
                        continue;
                    }
                    
                    TestExecuter executer = new TestExecuter(test);
                    Terminal.setInputList(executer.prepare());
                    
                    try {
                        runMain(new String[0]);   
                    } catch (InvocationTargetException e) {
                        executer.error();
                    }
                    
                    TestResult result = executer.compare();
                    
                    this.results.add(result);
                    
                    System.out.println(result);
                    
                    test.reset();
                }
            }
            
            System.out.println("========================");
        }
    }
    
    public void runMain(String[] args) throws InvocationTargetException {
        try {
            Class<?> clazz = Class.forName(Tester.MAIN);
            Method method = clazz.getMethod("main", String[].class);
            Object[] testArgs = new Object[1];
            testArgs[0] = args;
            method.invoke(null, testArgs);
        } catch (ClassNotFoundException e) {
            System.err.println("Class " + Tester.MAIN + " could not be found.");
        } catch (NoSuchMethodException | IllegalAccessException e) {
            System.err.println("No main method found in " + Tester.MAIN + ".");
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void printSummary() {
        int failed = 0, passed = 0, all = 0;
        
        System.out.println();
        
        for (TestResult result : results) {
            if (result.passed()) {
                passed++;
            } else {
                failed++;
            }
            
            all++;
        }
        
        System.out.println("You passed " + passed + "/" + all + " tests and failed " + failed + " test(s).");
    }
    
    private List<Method> sort(Class<?> clazz) {
        List<Method> methods = new ArrayList<>();

        for (int i = 0; i < clazz.getMethods().length; i++) {
            for (Method method : clazz.getMethods()) {
                if (!method.isAnnotationPresent(Test.class)) {
                    continue;
                }
                Test test = method.getAnnotation(Test.class);
                if (test.order() == i) {
                    methods.add(method);
                    continue;
                }
            }
        }
        for (Method method : clazz.getMethods()) {
            if (!method.isAnnotationPresent(Test.class)) {
                continue;
            }

            Test test = method.getAnnotation(Test.class);
            if (test.order() == -1) {
                methods.add(method);
                continue;
            }
        }

        return methods;
    }

    // http://snippets.dzone.com/posts/show/4831
    private static Class<?>[] getClasses(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        
        List<Class<?>> classes = new ArrayList<>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        
        return classes.toArray(new Class[classes.size()]);
    }

    private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<>();
        
        if (!directory.exists()) {
            return classes;
        }
        
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        
        return classes;
    }
}
