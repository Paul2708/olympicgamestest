package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-sports-venues logic test")
public class ListSportsVenuesLogicTest extends TestCase  {
    
    @Test(desc = "country has no ioc code", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expect("OK");
        print("list-sports-venues germany");
        expectError();
        quit(); 
    }
    
    @Test(desc = "no sports venues", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        print("list-sports-venues argentinien");
        quit(); 
    }
    
    @Test(desc = "sort the sports venues right", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;12000");
        expect("OK");
        print("add-sports-venue 002;argentinien;testplace;hall;1780;3200");
        expect("OK");
        print("add-sports-venue 003;argentinien;anotherplace;smallhall;1780;200");
        expect("OK");
        print("list-sports-venues argentinien");
        expect("(1 003 anotherplace 200)");
        expect("(2 002 testplace 3200)");
        expect("(3 001 moron 12000)");
        quit();
    }
    

    @Test(desc = "sort the sports venues right 2.0", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;12000");
        expect("OK");
        print("add-sports-venue 002;argentinien;testplace;hall;1780;3200");
        expect("OK");
        print("add-sports-venue 007;argentinien;testplace1;hall1;1770;3200");
        expect("OK");
        print("add-sports-venue 005;argentinien;testplace2;hall2;1760;3200");
        expect("OK");
        print("add-sports-venue 008;argentinien;anotherplace;smallhall;1780;200");
        expect("OK");
        print("list-sports-venues argentinien");
        expect("(1 008 anotherplace 200)");
        expect("(2 002 testplace 3200)");
        expect("(3 005 testplace2 3200)");
        expect("(4 007 testplace1 3200)");
        expect("(5 001 moron 12000)");
        quit();
    }

}
