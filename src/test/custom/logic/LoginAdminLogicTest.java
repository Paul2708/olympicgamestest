package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "login-admin logic test")
public class LoginAdminLogicTest extends TestCase {
    
    @Test(desc = "login and logout 3 times", order = 0)
    public void test1() {
        print("add-admin max;mustermann;maxi;123456789");
        expect("OK");
        print("login-admin maxi;123456789");
        expect("OK");
        print("logout-admin");
        expect("OK");
        print("login-admin maxi;123456789");
        expect("OK");
        print("logout-admin");
        expect("OK");
        print("add-admin maxi;musterfrau;maximus;otherpw123");
        expect("OK");
        print("login-admin maximus;otherpw123");
        expect("OK");
        print("logout-admin");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "username doesn't exist", order = 1)
    public void test2() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("login-admin maximus;12345678");
        expectError();
        quit(); 
    }
    
    @Test(desc = "wrong password", order = 2)
    public void test3() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("login-admin maxmus;1234567");
        expectError();
        quit(); 
    }
    
    @Test(desc = "admin already loged in", order = 3)
    public void test4() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        login();
        print("login-admin maxmus;12345678");
        expectError();
        print("logout-admin");
        expect("OK");
        print("login-admin maxmus;12345678");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "login at the beginning", order = 4)
    public void test5() {
        print("login-admin maxmus;nottherealpw");
        expectError();
        quit(); 
    }
}
