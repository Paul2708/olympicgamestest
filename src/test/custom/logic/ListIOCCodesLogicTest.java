package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-ioc-code logic test")
public class ListIOCCodesLogicTest extends TestCase {
    
    @Test(desc = "list-ioc-code sort test", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1984");
        expect("OK");
        print("add-ioc-code 113;bul;bulgarien;1896");
        expect("OK");
        print("add-ioc-code 114;chi;chile;1896");
        expect("OK");
        print("add-ioc-code 115;cze;tschechien;1992");
        expect("OK");
        print("add-ioc-code 116;ecu;ecuador;1924");
        expect("OK");
        print("add-ioc-code 117;esp;spanien;1900");
        expect("OK");
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("list-ioc-codes");
        expect("1896 113 bul bulgarien");
        expect("1896 114 chi chile");
        expect("1900 117 esp spanien");
        expect("1900 119 can kanada");
        expect("1920 111 arg argentinien");
        expect("1924 116 ecu ecuador");
        expect("1984 112 bhu bhutan");
        expect("1992 115 cze tschechien");
        expect("1992 118 ger deutschland");
        quit(); 
    }
    
    @Test(desc = "list-ioc-code with no ioc codes", order = 1)
    public void test2() {
        login();
        print("list-ioc-codes");
        quit(); 
    }
    
    @Test(desc = "list-ioc-codes simple test", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 113;bul;bulgarien;1896");
        expect("OK");
        print("add-ioc-code 114;chi;chile;1888");
        expect("OK");
        print("list-ioc-codes");
        expect("1888 114 chi chile");
        expect("1896 113 bul bulgarien");
        quit(); 
    }
    
    @Test(desc = "print ioc codes with leading zeros", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 010;ger;deutschland;1896");
        expect("OK");
        print("add-ioc-code 009;fra;frankreich;1742");
        expect("OK");
        print("add-ioc-code 123;pol;polen;0893");
        expect("OK");
        print("add-ioc-code 892;lop;lopland;0053");
        expect("OK");
        print("list-ioc-codes");
        
        expect("0053 892 lop lopland");
        expect("0893 123 pol polen");
        expect("1742 009 fra frankreich");
        expect("1896 010 ger deutschland");
        quit(); 
    }
}
