package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-sports-venue logic test")
public class AddSportsVenueLogicTest extends TestCase {
    
    @Test(desc = "id is already occupied", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1584");
        expect("OK");
        print("add-sports-venue 001;bhutan;nabot;smallplace;1820;120");
        expectError();
        print("add-sports-venue 123;bhutan;nabot;smallplace;1820;120");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "same parameters, different id", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expect("OK");
        print("add-sports-venue 003;argentinien;moron;greathall;1780;1200");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "country has no ioc code", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-sports-venue 001;deutschland;m�nchen;olympiahalle;12000");
        expectError();
        quit();
    }
    
    @Test(desc = "simple test", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1984");
        expect("OK");
        print("add-sports-venue 002;bhutan;test;testhall;1780;200");
        expect("OK");
        print("add-ioc-code 113;bul;bulgarien;1896");
        expect("OK");
        print("add-sports-venue 003;bulgarien;test;testhall;1780;12000");
        expect("OK");
        quit();
    }
}
