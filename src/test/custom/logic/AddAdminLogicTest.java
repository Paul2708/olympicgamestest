package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-admin logic test")
public class AddAdminLogicTest extends TestCase {
    
    @Test(desc = "login when someone is already loged in", order = 0)
    public void test1() {
        print("add-admin max;mustermann;maxmus;nottherealpw");
        expect("OK");
        login();
        print("login-admin maxmus;nottherealpw");
        expectError();
        quit(); 
    }
    
    @Test(desc = "login when someone is already loged in extended version", order = 1)
    public void test2() {
        print("add-admin max;mustermann;maxmus;nottherealpw");
        expect("OK");
        print("add-admin maxime;musterfrau;maximus;12345678");
        expect("OK");
        login();
        print("login-admin maxmus;nottherealpw");
        expectError();
        print("logout-admin");
        expect("OK");
        print("login-admin maximus;12345678");
        expect("OK");
        print("login-admin maxmus;nottherealpw");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add admin with same username", order = 2)
    public void test3() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("add-admin maxime;musterfrau;maxmus;otherpw123");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add admin with same name", order = 3)
    public void test4() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("add-admin max;mustermann2;maximus;otherpw123");
        expect("OK");
        print("add-admin maxi;mustermann;maxismus;other123pw");
        expect("OK");
        print("add-admin max;mustermann;marxmus;123otherpw");
        expect("OK");
        quit(); 
    } 
    
    @Test(desc = "add admin with same password", order = 4)
    public void test5() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("add-admin maxime;musterfrau;maximus;12345678");
        expect("OK");
        quit(); 
    }
}
