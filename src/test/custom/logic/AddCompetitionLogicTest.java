package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-competition logic test")
public class AddCompetitionLogicTest extends TestCase {
    
    @Test(desc = "add-competition simple test", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1934;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "add-competition id already occupied", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0001;1934;testland;eislauf;eisschnelllauf;1;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition country doesn't fit to athlete", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1934;deutschland;eislauf;eisschnelllauf;1;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition sport and discipline doesn't exists", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expectError();
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition sport and discipline doesn't fit to athlete", order = 4)
    public void test5() {
        login();

        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-olympic-sport boblauf;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1934;testland;boblauf;bob;1;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition two medals in the same year", order = 5)
    public void test6() {
        login();

        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1988;deutschland;eislauf;eisschnelllauf;0;0;1");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition two same medals in the same year other athlete", order = 6)
    public void test7() {
        login();

        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1930;kanada;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "add-competition wih invalid year", order = 7)
    public void test8() {
        login();
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1931;kanada;eislauf;eisschnelllauf;1;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition wih more than one medal 1.0", order = 8)
    public void test9() {
        login();
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1930;kanada;eislauf;eisschnelllauf;2;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-competition wih more than one medal 2.0", order = 9)
    public void test10() {
        login();
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1930;kanada;eislauf;eisschnelllauf;1;1;0");
        expectError();
        quit(); 
    }

}
