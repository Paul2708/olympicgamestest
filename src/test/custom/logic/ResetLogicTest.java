package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "reset logic test")
public class ResetLogicTest extends TestCase {

    @Test(desc = "clear sports", order = 0)
    public void test1() {
        login();
        addSport("test sport", "test discipline");
        addSport("abc", "123");
        addSport("def", "456");
        addSport("sporttype", "sportdiscipline");
        
        print("reset");
        expect("OK");
        
        print("list-olympic-sports");
        
        quit();
    }
    
    @Test(desc = "clear venues", order = 1)
    public void test2() {
        login();
        
        addCountry();
        addVenue("123");
        addVenue("456");
        addVenue("789");
        addVenue("666");
        
        print("reset");
        expect("OK");
        
        print("list-sports-venues testland");

        quit();
    }
    
    @Test(desc = "clear athletes", order = 2)
    public void test3() {
        login();
        
        addCountry();
        addSport("type", "discipline");
        addAthlete("1234", "type", "discipline");
        addAthlete("4569", "type", "discipline");
        addAthlete("7453", "type", "discipline");
        addAthlete("0123", "type", "discipline");
        
        print("reset");
        expect("OK");
        
        print("summary-athletes");

        quit();
    }
    
    @Test(desc = "clear competitions", order = 3)
    public void test4() {
        login();
        
        addCountry();
        addSport("type", "discipline");
        addAthlete("1234", "type", "discipline");
        addAthlete("4569", "type", "discipline");
        addAthlete("7453", "type", "discipline");
        addAthlete("0123", "type", "discipline");
        
        addCompetition("1234", "type", "discipline");
        addCompetition("4569", "type", "discipline");
        addCompetition("7453", "type", "discipline");
        addCompetition("0123", "type", "discipline");
        
        print("reset");
        expect("OK");
        
        print("olympic-medal-table");

        quit();
    }
    
    @Test(desc = "still logged-in", order = 4)
    public void test5() {
        print("add-admin max;mustermann;maxi;123456789");
        expect("OK");
        
        print("login-admin maxi;123456789");
        expect("OK");
        
        print("reset");
        expect("OK");
        
        print("add-admin leon;nameaufdemtaschenrechner;l_leon;123456789");
        expectError();

        quit();
    }
    
    @Test(desc = "remain users", order = 5)
    public void test6() {
        print("add-admin max;mustermann;maxi;123456789");
        expect("OK");
        print("add-admin lea;musterfrau;lea123;123456789");
        expect("OK");
        print("login-admin maxi;123456789");
        expect("OK");
        print("reset");
        expect("OK");
        print("logout-admin");
        expect("OK");
        print("login-admin lea123;123456789");
        expect("OK");
        quit();
    }
    
    private void addSport(String type, String discipline) {
        print("add-olympic-sport " + type + ";" + discipline);
        expect("OK");
    }
    
    private void addCountry() {
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
    }
    
    private void addVenue(String id) {
        print("add-sports-venue " + id + ";testland;testcity;name;1988;500000");
        expect("OK");
    }
    
    private void addAthlete(String id, String type, String discipline) {
        print("add-athlete " + id + ";max;mustermann;testland;" + type + ";" + discipline);
        expect("OK");
    }
    
    private void addCompetition(String id, String type, String discipline) {
        print("add-competition " + id + ";2018;testland;" + type + ";" + discipline + ";1;0;0");
        expect("OK");
    }
}