package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-athlete logic test")
public class AddAthleteLogicTest extends TestCase {
    
    @Test(desc = "add-athlete simple test", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eiskunstlauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "add-athlete id already occupied", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        print("add-athlete 0001;jane;doe;kanada;eislauf;eiskunstlauf");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-athlete country has no ioc code", order = 2)
    public void test3() {
        login();
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expectError();
        quit(); 
    }
    
    @Test(desc = "sport and discipline do not exists", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expectError();
        quit(); 
    }
    
    @Test(desc = "discipline do not exists", order = 4)
    public void test5() {
        login();
        print("add-olympic-sport bobsport;schnellbob");
        expect("OK");
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expectError();
        quit(); 
    }
    
    
}
