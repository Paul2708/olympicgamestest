package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-ioc-code logic test")
public class AddIOCCodeLogicTest extends TestCase {
    
    @Test(desc = "simple test for adding ioc codes", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1984");
        expect("OK");
        print("add-ioc-code 113;bul;bulgarien;1896");
        expect("OK");
        print("add-ioc-code 114;chi;chile;1896");
        expect("OK");
        print("add-ioc-code 115;cze;tschechien;1992");
        expect("OK");
        print("add-ioc-code 116;ecu;ecuador;1924");
        expect("OK");
        print("add-ioc-code 117;esp;spanien;1900");
        expect("OK");
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "add-ioc-code with same id", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1984");
        expect("OK");
        print("add-ioc-code 112;bul;bulgarien;1896");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-ioc-code with same ioc code", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 117;esp;spanien;1900");
        expect("OK");
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;esp;spantanen;1860");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-ioc-code with same country", order = 3)
    public void test4() {
        login();
        print("add-ioc-code 117;esp;spanien;1900");
        expect("OK");
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;erg;spanien;1860");
        expectError();
        quit(); 
    }

}
