package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-olympic-sports logic test")
public class ListOlympicSportsLogicTest extends TestCase {
    
    @Test(desc = "sort the olympic sports", order = 0)
    public void test1() {
        login();
        print("add-olympic-sport eishockey;eishockey");
        expect("OK");
        print("add-olympic-sport bobsport;skeleton");
        expect("OK");
        print("add-olympic-sport eislauf;eishockey");
        expect("OK");
        print("add-olympic-sport biathlon;biathlon");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport curling;curling");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expect("OK");
        print("list-olympic-sports");
        expect("biathlon biathlon");
        expect("bobsport bob");
        expect("bobsport skeleton");
        expect("curling curling");
        expect("eishockey eishockey");
        expect("eislauf eishockey");
        expect("eislauf eiskunstlauf");
        quit(); 
    }
    
    @Test(desc = "simple test", order = 1)
    public void test2() {
        login();
        print("add-olympic-sport eishockey;eishockey");
        expect("OK");
        print("list-olympic-sports");
        expect("eishockey eishockey");
        quit(); 
    }
    
    @Test(desc = "no olympic sports", order = 2)
    public void test3() {
        login();
        print("list-olympic-sports");
        quit();
    }
}
