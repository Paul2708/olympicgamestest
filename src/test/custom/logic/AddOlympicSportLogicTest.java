package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-olympic-sport logic test")
public class AddOlympicSportLogicTest extends TestCase {
    
    @Test(desc = "add olympic sport simple test", order = 0)
    public void test1() {
        login();
        print("add-olympic-sport eishockey;eishockey");
        expect("OK");
        print("add-olympic-sport biathlon;biathlon");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport curling;curling");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "disciplines for the same sport", order = 1)
    public void test2() {
        login();
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport bobsport;skeleton");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "same discipline for different sport", order = 2)
    public void test3() {
        login();
        print("add-olympic-sport eishockey;eishockey");
        expect("OK");
        print("add-olympic-sport eislauf;eishockey");
        expect("OK");
        quit();
    }
    
    @Test(desc = "same discipline and same sport exist", order = 3)
    public void test4() {
        login();
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-olympic-sport curling;curling");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expect("OK");
        print("add-olympic-sport eislauf;eiskunstlauf");
        expectError();
        quit();
    }

}
