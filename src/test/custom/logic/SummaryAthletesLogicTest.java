package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "summary-athlete logic test")
public class SummaryAthletesLogicTest extends TestCase {

    @Test(desc = "summary-athlete simple test", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("summary-athletes eislauf;eisschnelllauf");
        expect("0001 max mustermann 0");
        expect("0002 jane doe 0");
        expect("0003 nora jami 0");
        quit(); 
    }
    
    @Test(desc = "summary-athlete sort after medals", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1934;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("summary-athletes eislauf;eisschnelllauf");
        expect("0001 max mustermann 3");
        expect("0003 nora jami 2");
        expect("0002 jane doe 0");
        quit();  
    }
    
    @Test(desc = "summary-athlete sport doesn't exist", order = 2)
    public void test3() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("summary-athletes eislauf;eisschnelllauf");
        expectError();
        quit(); 
    }
    
    @Test(desc = "summary-athlete country has no ioc code", order = 3)
    public void test4() {
        login();
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("summary-athletes eislauf;eisschnelllauf");
        quit(); 
    }
    
    @Test(desc = "summary-athlete sort after medals and id", order = 4)
    public void test5() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-ioc-code 119;can;kanada;1900");
        expect("OK");
        print("add-ioc-code 120;tes;testland;1899");
        expect("OK");
        print("add-ioc-code 115;cze;tschechien;1992");
        expect("OK");
        print("add-olympic-sport eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0002;jane;doe;kanada;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0003;nora;jami;testland;eislauf;eisschnelllauf");
        expect("OK");
        print("add-athlete 0004;paula;esteban;tschechien;eislauf;eisschnelllauf");
        expect("OK");
        print("add-competition 0002;1926;kanada;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1930;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0001;1934;deutschland;eislauf;eisschnelllauf;0;0;0");
        expect("OK");
        print("add-competition 0001;1938;deutschland;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("add-competition 0001;1942;deutschland;eislauf;eisschnelllauf;0;0;1");
        expect("OK");
        print("add-competition 0003;1930;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0003;1934;testland;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0004;1934;tschechien;eislauf;eisschnelllauf;1;0;0");
        expect("OK");
        print("add-competition 0004;1938;tschechien;eislauf;eisschnelllauf;0;1;0");
        expect("OK");
        print("summary-athletes eislauf;eisschnelllauf");
        expect("0001 max mustermann 3");
        expect("0003 nora jami 2");
        expect("0004 paula esteban 2");
        expect("0002 jane doe 0");
        quit(); 
    }
}
