package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "quit logic test")
public class QuitLogicTest extends TestCase {

    @Test(desc = "sample test", order = 0)
    public void test1() {
        print("quit");
        expectQuit();
    }
    
    @Test(desc = "quit as admin", order = 1)
    public void test2() {
        login();
        
        print("quit");
        expectQuit();
        
        quit();
    }
}