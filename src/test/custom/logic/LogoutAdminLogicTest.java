package test.custom.logic;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "login-admin logic test")
public class LogoutAdminLogicTest extends TestCase {
    
    @Test(desc = "no user loged in", order = 0)
    public void test1() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("logout-admin");
        expectError();
        quit(); 
    }
    
    @Test(desc = "login and logout many times", order = 1)
    public void test2() {
        print("add-admin max;mustermann;maxmus;12345678");
        expect("OK");
        print("login-admin maxmus;12345678");
        expect("OK");
        logout();
        print("logout-admin");
        expectError();
        print("logout-admin");
        expectError();
        print("login-admin maxmus;12345678");
        expect("OK");
        logout();
        quit(); 
    }
    
    @Test(desc = "logout at the beginning", order = 2)
    public void test3() {
        print("logout-admin");
        expectError();
        quit(); 
    }

}
