package test.custom;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "not enough permissions test")
public class NotEnoughPermissionsTest extends TestCase {
    
    @Test(desc = "add-sports-venue with wrong permission", order = 0)
    public void test1() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        logout();
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expectError();
        quit(); 
    }
    
    @Test(desc = "list-sports-venues with wrong permission", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 111;arg;argentinien;1620");
        expect("OK");
        print("add-sports-venue 001;argentinien;moron;greathall;1780;1200");
        expect("OK");
        logout();
        print("list-sports-venues argentinien");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-olympic-sport with wrong permission", order = 2)
    public void test3() {
        login();
        logout();
        print("add-olympic-sport eishockey;eishockey");
        expectError();
        quit(); 
    }
    
    @Test(desc = "list-olympic-sports with wrong permission", order = 3)
    public void test4() {
        login();
        print("add-olympic-sport eishockey;eishockey");
        expect("OK");
        logout();
        print("list-olympic-sports");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-ioc-code with wrong permission", order = 4)
    public void test5() {
        print("add-ioc-code 111;arg;argentinien;1920");
        expectError();
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "list-ioc-codes with wrong permission", order = 5)
    public void test6() {
        login();
        print("add-ioc-code 111;arg;argentinien;1920");
        expect("OK");
        print("add-ioc-code 112;bhu;bhutan;1984");
        expect("OK");
        logout();
        print("list-ioc-codes");
        expectError();
        quit(); 
    }
    
    @Test(desc = "add-athlete with wrong permission", order = 6)
    public void test7() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        logout();
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expectError();
        print("login-admin maxi;123456789");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "summary-athletes with wrong permission", order = 7)
    public void test8() {
        login();
        print("add-ioc-code 118;ger;deutschland;1992");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        logout();
        print("summary-athletes bobsport;bob");
        quit(); 
    }
    
    @Test(desc = "add-competition with wrong permission", order = 8)
    public void test9() {
        login();
        print("add-ioc-code 118;ger;deutschland;1820");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        logout();
        print("add-competition 0001;1926;deutschland;bobsport;bob;1;0;0");
        expectError();
        quit(); 
    }
    
    @Test(desc = "olympic-medal-table with wrong permission", order = 9)
    public void test10() {
        login();
        print("add-ioc-code 118;ger;deutschland;1900");
        expect("OK");
        print("add-olympic-sport bobsport;bob");
        expect("OK");
        print("add-athlete 0001;max;mustermann;deutschland;bobsport;bob");
        expect("OK");
        print("add-competition 0001;1926;deutschland;bobsport;bob;1;0;0");
        expect("OK");
        logout();
        print("olympic-medal-table");
        expectError();
        quit(); 
    }
    
    @Test(desc = "reset with wrong permission", order = 10)
    public void test11() {
        print("reset");
        expectError();
        login();
        print("reset");
        expect("OK");
        quit(); 
    }
    
    @Test(desc = "quit with both permission 1.0", order = 11)
    public void test12() {   
        quit(); 
    }
    
    @Test(desc = "reset with both permission 2.0", order = 12)
    public void test13() {
        login();
        quit(); 
    }
}
