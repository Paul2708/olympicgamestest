package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-athlete semantics test")
public class AddAthleteSemanticsTest extends TestCase {

    @Test(desc = "invalid id", order = 0)
    public void test1() {
        login();
        addCountryAndSport();
        
        print("add-athlete 0000;max;mustermann;testland;type;discipline");
        expectError();
        print("add-athlete 123;max;mustermann;testland;type;discipline");
        expectError();
        print("add-athlete 12;max;mustermann;testland;type;discipline");
        expectError();
        print("add-athlete 1;max;mustermann;testland;type;discipline");
        expectError();
        print("add-athlete 009;max;mustermann;testland;type;discipline");
        expectError();
        print("add-athlete 12345;max;mustermann;testland;type;discipline");
        expectError();
        
        quit();
    }
    
    @Test(desc = "whitespace as name", order = 1)
    public void test2() {
        login();
        addCountryAndSport();
        
        print("add-athlete 0001; ;mustermann;testland;type;discipline");
        expect("OK");
        print("add-athlete 0002;max; ;testland;type;discipline");
        expect("OK");
        print("add-athlete 0003;   ;    ;testland;type;discipline");
        expect("OK");
        
        quit();
    }
    
    private void addCountryAndSport() {
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport type;discipline");
        expect("OK");
    }
}
