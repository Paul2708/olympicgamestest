package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-competition semantics test")
public class AddCompetitionSemanticsTest extends TestCase {

    @Test(desc = "invalid athlete id", order = 0)
    public void test1() {
        login();
        addAthlete();
        
        print("add-competition 01234;2018;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 123;2018;testland;type;discipline;1;0;0");
        expectError();
        
        quit();
    }
    
    @Test(desc = "invalid year", order = 1)
    public void test2() {
        login();
        addAthlete();
        
        print("add-competition 1234;1920;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;0020;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;1922;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;2019;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;2020;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;1956;testland;type;discipline;1;0;0");
        expectError();
        print("add-competition 1234;1951;testland;type;discipline;1;0;0");
        expectError();
        
        quit();
    }
    
    @Test(desc = "invalid medals", order = 2)
    public void test3() {
        login();
        addAthlete();
        
        print("add-competition 1234;2018;testland;type;discipline;1;1;0");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;1;0;1");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;1;1;1");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;0;1;1");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;20;0;0");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;0;-1;0");
        expectError();
        print("add-competition 1234;2018;testland;type;discipline;4;1;2");
        expectError();
        
        quit();
    }
    
    private void addAthlete() {
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
        print("add-olympic-sport type;discipline");
        expect("OK");
        print("add-athlete 1234;max;mustermann;testland;type;discipline");
        expect("OK");
    }
}
