package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-ioc-code semantics test")
public class AddIOCCodeSemanticsTest extends TestCase {

    @Test(desc = "invalid id", order = 0)
    public void test1() {
        login();
        
        print("add-ioc-code 000;tes;testland;1899");
        expectError();
        print("add-ioc-code 10;tes;testland;1899");
        expectError();
        print("add-ioc-code 1;tes;testland;1899");
        expectError();
        print("add-ioc-code 1598;tes;testland;1899");
        expectError();
        print("add-ioc-code 1000;tes;testland;1899");
        expectError();
        print("add-ioc-code 09;tes;testland;1899");
        expectError();
        
        quit();
    }
    
    @Test(desc = "invalid ioc code", order = 1)
    public void test2() {
        login();
        
        print("add-ioc-code 123;test;testland;1899");
        expectError();
        print("add-ioc-code 123;testland;testland;1899");
        expectError();
        print("add-ioc-code 123;te;testland;1899");
        expectError();
        print("add-ioc-code 123;?!a;testland;1899");
        expectError();
        print("add-ioc-code 123;   ;testland;1899");
        expectError();
        print("add-ioc-code 123;ab2;testland;1899");
        expectError();
        
        quit();
    }
    
    @Test(desc = "invalid year", order = 2)
    public void test3() {
        login();
        
        print("add-ioc-code 123;tes;testland;213");
        expectError();
        print("add-ioc-code 123;tes;testland;-2018");
        expectError();
        print("add-ioc-code 123;tes;testland;012");
        expectError();
        print("add-ioc-code 123;tes;testland;2");
        expectError();
        print("add-ioc-code 123;tes;testland;0000");
        expectError();
        
        quit();
    } 
}
