package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "login-admin semantics test")
public class LoginAdminSemanticsTest extends TestCase {

    @Test(desc = "login with whitespace", order = 0)
    public void test1() {
        print("add-admin    max;mustermann;maxi  ;123456789");
        expect("OK");
        print("add-admin max;mustermann;    ;123456789");
        expect("OK");
        
        print("login-admin maxi  ;123456789");
        expect("OK");
        logout();
        print("login-admin     ;123456789");
        expect("OK");
        logout();
        
        quit();
    }
    
    @Test(desc = "login with special chars", order = 1)
    public void test2() {
        print("add-admin #test;##?;`?`%;123456789");
        expect("OK");
        print("add-admin max;mustermann;maxi;?!\"�$%&/(");
        expect("OK");
        
        print("login-admin `?`%;123456789");
        expect("OK");
        logout();
        print("login-admin maxi;?!\"�$%&/(");
        expect("OK");
        logout();
        
        quit();
    }
}
