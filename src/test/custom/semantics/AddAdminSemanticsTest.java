package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-admin semantics test")
public class AddAdminSemanticsTest extends TestCase {

    @Test(desc = "whitespace as name", order = 0)
    public void test1() {
        print("add-admin    max;mustermann;maxi  ;123456789");
        expect("OK");
        print("add-admin max;mustermann;    ;123456789");
        expect("OK");
        print("add-admin  ; ;      ;123456789");
        expect("OK");
        quit();
    }
    
    @Test(desc = "special characters as name and password", order = 1)
    public void test2() {
        print("add-admin #test;##?;`?`%;123456789");
        expect("OK");
        print("add-admin #*#test~;name;username;123456789");
        expect("OK");
        print("add-admin max;mustermann;maxi;?!\"�$%&/(");
        expect("OK");
        quit();
    }
    
    @Test(desc = "too many chars as username", order = 2)
    public void test3() {
        print("add-admin max;mustermann;123456789;123456789");
        expectError();
        print("add-admin #*#test~;name;thatsaverylongusernamekappa;123456789");
        expectError();
        quit();
    }
    
    @Test(desc = "less chars as username", order = 3)
    public void test4() {
        print("add-admin max;mustermann;123;123456789");
        expectError();
        print("add-admin #*#test~;name;ups;123456789");
        expectError();
        quit();
    }
    
    @Test(desc = "too many chars as password", order = 4)
    public void test5() {
        print("add-admin max;mustermann;maxi;1234567890123");
        expectError();
        print("add-admin max;mustermann;maxi;thatsthebestpasswordeverlulspamrogergergerg");
        expectError();
        quit();
    }
    
    @Test(desc = "less chars as password", order = 5)
    public void test6() {
        print("add-admin max;mustermann;maxi;1234567");
        expectError();
        print("add-admin max;mustermann;maxi;short");
        expectError();
        quit();
    }
}
