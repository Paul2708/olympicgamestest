package test.custom.semantics;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-sports-venue semantics test")
public class AddSportsVenueSemanticsTest extends TestCase {

    @Test(desc = "invalid id", order = 0)
    public void test1() {
        login();
        addCountry();
        
        print("add-sports-venue 000;testland;testcity;testarena;1899;750000");
        expectError();
        print("add-sports-venue 10;testland;testcity;testarena;1899;750000");
        expectError();
        print("add-sports-venue 1000;testland;testcity;testarena;1899;750000");
        expectError();
        print("add-sports-venue 6;testland;testcity;testarena;1899;750000");
        expectError();
        print("add-sports-venue 4686;testland;testcity;testarena;1899;750000");
        expectError();
        print("add-sports-venue 09;testland;testcity;testarena;1899;750000");
        expectError();
        
        quit();
    }
    
    @Test(desc = "invalid year", order = 1)
    public void test2() {
        login();
        addCountry();
        
        print("add-sports-venue 123;testland;testcity;testarena;213;750000");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;-2018;750000");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;012;750000");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;2;750000");
        expectError();
        
        quit();
    }
    
    @Test(desc = "negative amount of seats", order = 2)
    public void test3() {
        login();
        addCountry();
        
        print("add-sports-venue 123;testland;testcity;testarena;213;-1");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;-2018;-4565956464692154");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;012;-123");
        expectError();
        print("add-sports-venue 123;testland;testcity;testarena;2;750000");
        expectError();
        
        quit();
    }
    
    @Test(desc = "zero seats", order = 3)
    public void test4() {
        login();
        addCountry();
        
        print("add-sports-venue 123;testland;testcity;testarena;2001;0");
        expect("OK");
        
        quit();
    }
    
    private void addCountry() {
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
    }
}
