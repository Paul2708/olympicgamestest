package test.custom;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "Praktomat test")
public class PublicTest extends TestCase {

    @Test(desc = "public test")
    public void test() {
        print("add-admin peter;oettig;peet1993;notmyrealpw");
        expect("OK");
        print("login-admin peet1993;notmyrealpw");
        expect("OK");
        print("add-olympic-sport biathlon;sprint");
        expect("OK");
        print("add-ioc-code 111;ger;deutschland;1990");
        expect("OK");
        print("add-athlete 1234;laura;dahlmeier;deutschland;biathlon;sprint");
        expect("OK");
        print("add-competition 1234;2018;deutschland;biathlon;sprint;1;0;0");
        expect("OK");
        print("summary-athletes biathlon;sprint");
        expect("1234 laura dahlmeier 1");
        print("olympic-medal-table");
        expect("(1,111,ger,deutschland,1,0,0,1)");
        print("quit");
        expectQuit();
    }
}
