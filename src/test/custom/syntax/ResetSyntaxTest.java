package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "reset syntax test")
public class ResetSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("reset ");
        expectError();
        print("reset              ");
        expectError();
        print("reset    ");
        expectError();
        print("reset            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("reset ;");
        expectError();
        print("reset test;test");
        expectError();
        print("reset ;test;");
        expectError();
        print("reset test   ;");
        expectError();
        quit();
    }
}

