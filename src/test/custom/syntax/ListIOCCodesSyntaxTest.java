package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-ioc-codes syntax test")
public class ListIOCCodesSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("list-ioc-codes ");
        expectError();
        print("list-ioc-codes              ");
        expectError();
        print("list-ioc-codes    ");
        expectError();
        print("list-ioc-codes            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("list-ioc-codes ;");
        expectError();
        print("list-ioc-codes country;a");
        expectError();
        print("list-ioc-codes ;country;");
        expectError();
        print("list-ioc-codes country   ;");
        expectError();
        quit();
    }
}

