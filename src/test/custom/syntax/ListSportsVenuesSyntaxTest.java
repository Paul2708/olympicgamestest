package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-sports-venues syntax test")
public class ListSportsVenuesSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        
        print("list-sports-venues");
        expectError();
        print("list-sports-venues ");
        expectError();
        
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("list-sports-venues country;");
        expectError();
        print("list-sports-venues ;country");
        expectError();
        print("list-sports-venues ;country;");
        expectError();
        print("list-sports-venues country   ;");
        expectError();
        quit();
    }
}

