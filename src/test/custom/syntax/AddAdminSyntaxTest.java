package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-admin syntax test")
public class AddAdminSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        print("add-admin");
        expectError();
        print("add-admin ");
        expectError();
        print("add-admin              ");
        expectError();
        print("add-admin    ");
        expectError();
        print("add-admin            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        print("add-admin firstname;lastname;nickname;12345678;");
        expectError();
        print("add-admin ");
        expectError();
        print("add-admin ;lastname;nickname;12345678;");
        expectError();
        print("add-admin first;last;");
        expectError();
        print("add-admin name;name;;");
        expectError();
        print("add-admin ;name;name;nickname;123456789");
        expectError();
        quit();
    }
    
    // no invalid arguments test, because all input args can be valid
}

