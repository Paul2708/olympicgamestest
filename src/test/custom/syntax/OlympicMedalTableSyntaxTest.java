package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "olympic-medal-table syntax test")
public class OlympicMedalTableSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("olympic-medal-table ");
        expectError();
        print("olympic-medal-table              ");
        expectError();
        print("olympic-medal-table    ");
        expectError();
        print("olympic-medal-table            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("olympic-medal-table ;");
        expectError();
        print("olympic-medal-table test;test");
        expectError();
        print("olympic-medal-table ;test;");
        expectError();
        print("olympic-medal-table test   ;");
        expectError();
        quit();
    }
}

