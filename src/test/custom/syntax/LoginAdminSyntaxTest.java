package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "login-admin syntax test")
public class LoginAdminSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        print("login-admin");
        expectError();
        print("login-admin ");
        expectError();
        print("login-admin              ");
        expectError();
        print("login-admin    ");
        expectError();
        print("login-admin            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        print("login-admin username;password;");
        expectError();
        print("login-admin ;username;password");
        expectError();
        print("login-admin ;username;password;");
        expectError();
        print("login-admin username   ;");
        expectError();
        print("login-admin username;;password");
        expectError();
        quit();
    }
    
    // no invalid arguments test, because all input args can be valid
}

