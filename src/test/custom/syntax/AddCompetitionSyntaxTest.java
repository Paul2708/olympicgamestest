package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-competition syntax test")
public class AddCompetitionSyntaxTest extends TestCase {
    
    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("add-olympic-sport ice;snow");
        expect("OK");
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
        print("add-athlete 1234;max;mustermann;testland;ice;snow");
        expect("OK");
        
        print("add-competition");
        expectError();
        print("add-competition ");
        expectError();
        print("add-competition              ");
        expectError();
        print("add-competition    ");
        expectError();
        print("add-competition            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("add-olympic-sport ice;snow");
        expect("OK");
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
        print("add-athlete 1234;max;mustermann;testland;ice;snow");
        expect("OK");
        
        print("add-competition 1234;1930;testland;ice;snow;1");
        expectError();
        print("add-competition ;1234;1930;testland;ice;snow;1;0;0");
        expectError();
        print("add-competition testland;ice;snow;1;0;0");
        expectError();
        print("add-competition 1234;1930;testland;ice;snow;1;0;0;0");
        expectError();
        print("add-competition 1234;1930;;ice;snow;1;0;0");
        expectError();
        quit();
    }
    
    @Test(desc = "invalid arguments", order = 2)
    public void test3() {
        login();
        print("add-olympic-sport ice;snow");
        expect("OK");
        print("add-ioc-code 123;tes;testland;1899");
        expect("OK");
        print("add-athlete 1234;max;mustermann;testland;ice;snow");
        expect("OK");
        
        print("add-competition abcd;1930;testland;ice;snow;1;0;0");
        expectError();
        print("add-competition 1234;abcd;testland;ice;snow;1;0;0");
        expectError();
        print("add-competition 1234;1930;testland;ice;snow;a;b;c");
        expectError();
        print("add-competition 1234;1930;testland;ice;snow;a;0;x");
        expectError();
        print("add-competition 1234;1930;testland;ice;snow;b;2;0");
        expectError();
        quit();
    }
}

