package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-athlete syntax test")
public class AddAthleteSyntaxTest extends TestCase {
        
    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("add-athlete");
        expectError();
        print("add-athlete ");
        expectError();
        print("add-athlete              ");
        expectError();
        print("add-athlete    ");
        expectError();
        print("add-athlete            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("add-athlete 1234;max;mustermann;testland;ice;snow;");
        expectError();
        print("add-athlete ;1234;max;mustermann;testland;ice;snow");
        expectError();
        print("add-athlete ;1234;max;mustermann;testland;ice;snow;");
        expectError();
        print("add-athlete 1234;max;;testland;ice;snow");
        expectError();
        print("add-athlete ;max;mustermann;testland;ice;snow");
        expectError();
        print("add-athlete 1234;max;mustermann;testland;snow");
        expectError();
        quit();
    }
    
    @Test(desc = "invalid arguments", order = 2)
    public void test3() {
        login();
        print("add-athlete abcd;max;mustermann;testland;ice;snow");
        expectError();
        quit();
    }
}

