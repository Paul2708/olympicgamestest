package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "logout-admin syntax test")
public class LogoutAdminSyntaxTest extends TestCase {

    @Test(desc = "no arguments (whitespace)", order = 0)
    public void test1() {
        login();
        print("logout-admin ");
        expectError();
        print("logout-admin              ");
        expectError();
        print("logout-admin    ");
        expectError();
        print("logout-admin            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("logout-admin name");
        expectError();
        print("logout-admin ;name");
        expectError();
        print("logout-admin ;name;");
        expectError();
        print("logout-admin username   ;");
        expectError();
        print("logout-admin username;;password");
        expectError();
        quit();
    }
}

