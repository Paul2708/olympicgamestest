package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "summary-athletes syntax test")
public class SummaryAthletesSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("summary-athletes");
        expectError();
        print("summary-athletes ");
        expectError();
        print("summary-athletes              ");
        expectError();
        print("summary-athletes    ");
        expectError();
        print("summary-athletes            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("summary-athletes type;discipline;test");
        expectError();
        print("summary-athletes ;type;discipline");
        expectError();
        print("summary-athletes type");
        expectError();
        print("summary-athletes ;   type;discipline;");
        expectError();
        quit();
    }
}

