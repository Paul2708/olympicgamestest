package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-sports-venue syntax test")
public class AddSportsVenueSyntaxTest extends TestCase {
    
    // add-sports-venue <ID>;<L�ndername>;<Ort>;<Name>;<Er�ffnungsjahr>;<Anzahl_Sitzpl�tze>
    
    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("add-sports-venue");
        expectError();
        print("add-sports-venue ");
        expectError();
        print("add-sports-venue              ");
        expectError();
        print("add-sports-venue    ");
        expectError();
        print("add-sports-venue            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("add-sports-venue 666;testland;testcity;test;1899;1337;");
        expectError();
        print("add-sports-venue ;122;testland;testcity;test;1899;1337;");
        expectError();
        print("add-sports-venue 111;testland;;test;1899;1337;");
        expectError();
        print("add-sports-venue 662;testland;testcity;test;1899;adada;");
        expectError();
        print("add-sports-venue ;;");
        expectError();
        print("add-sports-venue 565;a;b;c;d;;;;");
        expectError();
        quit();
    }
    
    @Test(desc = "invalid arguments", order = 2)
    public void test3() {
        login();
        print("add-sports-venue abc;testland;testcity;test;1899;1337");
        expectError();
        print("add-sports-venue 666;testland;testcity;test;abcd;1337");
        expectError();
        print("add-sports-venue 666;testland;testcity;test;1899;manyseats");
        expectError();
        quit();
    }
}

