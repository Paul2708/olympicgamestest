package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "list-olympic-sports syntax test")
public class ListOlympicSportsSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("list-olympic-sports ");
        expectError();
        print("list-olympic-sports              ");
        expectError();
        print("list-olympic-sports    ");
        expectError();
        print("list-olympic-sports            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("list-olympic-sports country;");
        expectError();
        print("list-olympic-sports ;country");
        expectError();
        print("list-olympic-sports ;country;");
        expectError();
        print("list-olympic-sports country   ;");
        expectError();
        quit();
    }
}

