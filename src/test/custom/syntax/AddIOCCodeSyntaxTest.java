package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-ioc-code syntax test")
public class AddIOCCodeSyntaxTest extends TestCase {
    
    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("add-ioc-code");
        expectError();
        print("add-ioc-code ");
        expectError();
        print("add-ioc-code              ");
        expectError();
        print("add-ioc-code    ");
        expectError();
        print("add-ioc-code            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("add-ioc-code 123;tes;testland;1899;");
        expectError();
        print("add-ioc-code ;123;tes;testland;1899");
        expectError();
        print("add-ioc-code ;123;tes;testland;1899;");
        expectError();
        print("add-ioc-code 123;tes;;1899");
        expectError();
        print("add-ioc-code 123;;;testland;1899");
        expectError();
        print("add-ioc-code 123;tes;1899");
        expectError();
        quit();
    }
    
    @Test(desc = "invalid arguments", order = 2)
    public void test3() {
        login();
        print("add-ioc-code abc;tes;testland;1899");
        expectError();
        print("add-ioc-code 123;123;testland;1899");
        expectError();
        print("add-ioc-code 123;tes;testland;abcd");
        expectError();
        quit();
    }
}

