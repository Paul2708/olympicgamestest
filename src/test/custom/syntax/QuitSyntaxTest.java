package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "quit syntax test")
public class QuitSyntaxTest extends TestCase {

    @Test(desc = "no arguments", order = 0)
    public void test1() {
        print("quit ");
        expectError();
        print("quit              ");
        expectError();
        print("quit    ");
        expectError();
        print("quit            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        print("quit ;");
        expectError();
        print("quit test;test");
        expectError();
        print("quit test");
        expectError();
        print("quit test   ;");
        expectError();
        quit();
    }
}

