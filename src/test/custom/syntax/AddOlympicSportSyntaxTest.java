package test.custom.syntax;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "add-olympic-sport syntax test")
public class AddOlympicSportSyntaxTest extends TestCase {
    
    @Test(desc = "no arguments", order = 0)
    public void test1() {
        login();
        print("add-olympic-sport");
        expectError();
        print("add-olympic-sport ");
        expectError();
        print("add-olympic-sport              ");
        expectError();
        print("add-olympic-sport    ");
        expectError();
        print("add-olympic-sport            ");
        expectError();
        quit();
    }
    
    @Test(desc = "wrong number of arguments", order = 1)
    public void test2() {
        login();
        print("add-olympic-sport type;discipline;");
        expectError();
        print("add-olympic-sport ;type;discipline");
        expectError();
        print("add-olympic-sport type;");
        expectError();
        print("add-olympic-sport sport type; sport discipline   ;");
        expectError();
        print("add-olympic-sport ;;");
        expectError();
        quit();
    }
}

