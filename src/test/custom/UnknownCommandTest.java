package test.custom;

import test.TestCase;
import test.annotation.Test;
import test.annotation.TestUnit;

@TestUnit(header = "Unknown commands test")
public class UnknownCommandTest extends TestCase {

    @Test(desc = "unknown commands")
    public void test1() {
        print("test");
        expectError();
        print("a");
        expectError();
        print("rgreg5e4rg1r2eg54reg6rg1r");
        expectError();
        quit();
    }
    
    @Test(desc = "unknown commands with ';'", order = 1)
    public void test2() {
        print("rgerg grgr ;egrrg; grgrg");
        expectError();
        print("; ;");
        expectError();
        quit();
    }
    
    @Test(desc = "unknown commands with whitespace", order = 2)
    public void test3() {
        print("    ");
        expectError();
        print("");
        expectError();
        print("             ");
        expectError();
        quit();
    }
}
