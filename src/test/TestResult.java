package test;

import java.util.ArrayList;
import java.util.List;

public class TestResult {

    private Class<?> testClass;
    private long time;
    private List<Failure> failures;
    
    public TestResult(Class<?> testClass) {
        this.testClass = testClass;
        
        this.failures = new ArrayList<>();
    }
    
    public void setTime(long time) {
        this.time = time;
    }
    
    public void addFailure(Failure failure) {
        this.failures.add(failure);
    }
    
    public boolean passed() {
        return failures.size() == 0;
    }
    
    @Override
    public String toString() {
        String result = "";
        
        if (passed()) {
            result = "  Test passed. (" + time + "ms)";
        } else {
            result += " Test failed. (" + time + "ms)" + "\n";
            result += "     Lookup: " + testClass.getName() + "\n";
            for (int i = 0; i < failures.size(); i++) {
                Failure failure = failures.get(i);
                
                result += "     Line: " + failure.line + "\n";
                if (failure.expected.contains("\n")) {
                    result += "     Expected: \n" + failure.expected + "\n";
                } else {
                    result += "     Expected: " + failure.expected + "\n";
                }
                if (failure.found.contains("\n")) {
                    result += "     Found: \n" + failure.found;
                } else {
                    result += "     Found: " + failure.found;
                }
                
                if (i < failures.size() - 1) {
                    result += "\n";
                }
            }
        }
        
        return result;
    }
    
    public static class Failure {
        
        private String line;
        private String expected;
        private String found;
        
        public Failure(String line, String expected, String found) {
            this.line = line;
            this.expected = expected;
            this.found = found;
        }
    }
}
