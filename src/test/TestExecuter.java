package test;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import edu.kit.informatik.Terminal;
import test.TestExpectation.Type;
import test.TestResult.Failure;

public class TestExecuter {

    private TestCase testCase;

    private List<TestExpectation> expectations;
    private Instant startTime;
    private boolean error;

    public TestExecuter(TestCase testCase) {
        this.testCase = testCase;

        this.expectations = testCase.getExpectations();
        this.startTime = Instant.now();
        this.error = false;
    }

    public List<String> prepare() {
        // System.out.println("Prepare...");

        List<String> list = new ArrayList<>();

        for (TestExpectation expectation : expectations) {
            if (expectation.getCommand() == null) {
                continue;
            }

            list.add(expectation.getCommand());
        }

        // System.out.println("Total line: " + list.toString());

        return list;
    }

    public TestResult compare() {
        // System.out.println("Compare...");

        TestResult result = new TestResult(testCase.getClass());
        
        int outputIndex = 0;
        for (TestExpectation expectation : expectations) {
            TestExpectation.Type expectedType = expectation.getExpectedType();
            
            // Quit
            if (expectedType == Type.QUIT) {
                if (error) {
                    result.addFailure(new Failure(expectation.getCommand(), "quit", "another Terminal.readLine() call"));
                }

                continue;
            }
            // Error
            if (expectedType == Type.ERROR) {
                if (outputIndex >= Terminal.getCurrentOutput().size()) {
                    result.addFailure(new Failure(expectation.getCommand(), "error message", "nothing -/-"));
                } else {
                    String found = Terminal.getCurrentOutput().get(outputIndex);
                    if (!found.startsWith("Error, ")) {
                        result.addFailure(new Failure(expectation.getCommand(), "error message", found));
                    }
                    
                    outputIndex++;
                }

                continue;
            }
            // Text
            if (expectedType == Type.TEXT) {
                if (outputIndex >= Terminal.getCurrentOutput().size()) {
                    result.addFailure(new Failure(expectation.getCommand(), expectation.getExpectedText(), "nothing -/-"));
                } else {
                    String found = Terminal.getCurrentOutput().get(outputIndex);
                    if (!expectation.getExpectedText().equals(found)) {
                        result.addFailure(new Failure(expectation.getCommand(), expectation.getExpectedText(), found));
                    }
                    
                    outputIndex++;
                }
            }
        }

        Duration duration = Duration.between(startTime, Instant.now());
        result.setTime(duration.toMillis());

        return result;
    }

    public void error() {
        this.error = true;
    }
}
