package test;

public class Tester {
    
    public static final String MAIN = "de.paul2708.olympic.Main";
    
    public static void main(String[] args) {
        TestManager manager = new TestManager();
        
        manager.loadTestCases();
        manager.runTestCases();
        manager.printSummary();
    }
}
