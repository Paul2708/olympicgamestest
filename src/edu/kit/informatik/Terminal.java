package edu.kit.informatik;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public final class Terminal {
    
    private static boolean testing;
    
    private static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in));
    
    private static int index;
    private static List<String> currentInput;
    private static List<String> currentOutput;
    
    static {
        Terminal.testing = false;
        Terminal.currentOutput = new ArrayList<>();
    }
    
    /*
     * Methods for testing
     */
    public static void testing() {
        Terminal.testing = true;
    }
    
    public static void setInputList(List<String> list) {
        Terminal.currentInput = list;
        Terminal.index = 0;
        Terminal.currentOutput.clear();
    }
    
    public static List<String> getCurrentOutput() {
        return currentOutput;
    }
    
    /*
     * Original terminal methods
     */
    public static void printError(final String message) {
        Terminal.printLine("Error, " + message);
    }

    public static void printLine(final Object object) {
        if (Terminal.testing) {
            String toString = object.toString();
            
            if (toString.contains("\n")) {
                for (String line : toString.split("\n")) {
                    Terminal.currentOutput.add(line);
                }
                if (toString.endsWith("\n")) {
                    Terminal.currentOutput.add("");
                }
            } else {
                Terminal.currentOutput.add(toString);
            }
        } else {
            System.out.println(object);
        }
    }

    public static void printLine(final char[] charArray) {
        if (Terminal.testing) {
            String toString = String.valueOf(charArray);
            
            if (toString.contains("\n")) {
                for (String line : toString.split("\n")) {
                    Terminal.currentOutput.add(line);
                }
                if (toString.endsWith("\n")) {
                    Terminal.currentOutput.add("");
                }
            } else {
                Terminal.currentOutput.add(toString);
            }
        } else {
            System.out.println(charArray);
        }
    }
    
    public static String readLine() {
        if (Terminal.testing) {
            String output = Terminal.currentInput.get(index);
            Terminal.index++;
            
            return output;
        } else {
            try {
                return IN.readLine();
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    public static String[] readFile(final String path) {
        try (final BufferedReader reader = new BufferedReader(new FileReader(path))) {
            return reader.lines().toArray(String[]::new);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}